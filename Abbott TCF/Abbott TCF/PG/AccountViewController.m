//
//  AccountViewController.m
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/10/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import "AccountViewController.h"
#import "UIScrollView+ScrollIndicator.h"
#import "AppDelegate.h"


@interface AccountViewController ()

@end

@implementation AccountViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSDictionary *underlineAttribute = @{NSForegroundColorAttributeName: [UIColor colorWithRed:180/255.0 green:25/255.0 blue:137/255.0 alpha:1.0],
                                         NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    
    self.accountLabel.attributedText = [[NSAttributedString alloc] initWithString:@"PG001" attributes:underlineAttribute];
    self.countLabel.attributedText = [[NSAttributedString alloc] initWithString:@"123" attributes:underlineAttribute];
    
    NSDictionary *underlineAttribute2 = @{NSForegroundColorAttributeName: [UIColor colorWithRed:86/255.0 green:86/255.0 blue:86/255.0 alpha:1.0],
                                          NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),
                                          NSFontAttributeName: [UIFont italicSystemFontOfSize:15.0]};
    [self.logoutButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Logout" attributes:underlineAttribute2] forState:UIControlStateNormal];
    
    [self.detailScrollView setContentSize:CGSizeMake(self.detailScrollView.frame.size.width, 1460)];
    [self.detailScrollView enableCustomScrollIndicatorsWithScrollIndicatorType:JMOScrollIndicatorTypeClassic positions:JMOVerticalScrollIndicatorPositionRight color:[UIColor colorWithRed:180/255.0 green:25/255.0 blue:137/255.0 alpha:1.0]];

    [self.listScrollView enableCustomScrollIndicatorsWithScrollIndicatorType:JMOScrollIndicatorTypeClassic positions:JMOVerticalScrollIndicatorPositionRight color:[UIColor colorWithRed:180/255.0 green:25/255.0 blue:137/255.0 alpha:1.0]];
    
    [self.listScrollView setHidden:NO];
    [self.detailScrollView setHidden:YES];

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Customer"inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setResultType:NSDictionaryResultType];
    NSError *error;
    customers = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    
    [self generateList];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) generateList {
    [[self.listScrollView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    for (UIViewController *viewController in self.childViewControllers) {
        if ([viewController isKindOfClass:[CustomerItemViewController class]]) {
            [viewController didMoveToParentViewController:nil];
            [viewController removeFromParentViewController];
        }
    }
    
    float x = 13;
    float y = 13;
    float itemHeight = 0;
    for (NSDictionary *dict in customers) {
        CustomerItemViewController *item = [[CustomerItemViewController alloc] initWithNibName:@"CustomerItemViewController" bundle:nil];
        item.customer = dict;
        item.view.frame = CGRectMake(x, y, item.view.frame.size.width, item.view.frame.size.height);
        
        x += item.view.frame.size.width + 13;
        if (x + item.view.frame.size.width >= self.listScrollView.frame.size.width) {
            x = 13;
            y += item.view.frame.size.height + 13;
        }
        
        itemHeight = item.view.frame.size.height;
        
        item.delegate = self;
        
        [self addChildViewController:item];
        [self.listScrollView addSubview:item.view];
        [self didMoveToParentViewController:self];
    }
    
    [self.agreement1Button setSelected:YES];
    [self.agreement2Button setSelected:YES];
    [self.agreement3Button setSelected:YES];
    
    [self.listScrollView setContentSize:CGSizeMake(self.listScrollView.frame.size.width, y + itemHeight + 13)];
}

- (IBAction)logoutButtonTouched:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)backButtonTouched:(id)sender {
    [self.detailScrollView setHidden:YES];
    [self.listScrollView setHidden:NO];
    [self.backButton setHidden:YES];
}

- (IBAction)updateButtonTouched:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate deleteAll];
    
    customers = [[NSArray alloc] init];
    [self generateList];
}


- (void) showDetail:(NSDictionary *)item {
    [self.detailScrollView setHidden:NO];
    [self.listScrollView setHidden:YES];
    [self.backButton setHidden:NO];
    
    [self.detailScrollView setContentOffset:CGPointMake(0, 0)];
    
    self.avatarImageView.image = [UIImage imageWithData:(NSData *)[item valueForKey:@"picture"]];
    self.signatureImageView.image = [UIImage imageWithData:(NSData *)[item valueForKey:@"signature"]];
    self.motherNameTextField.text = [item valueForKey:@"mother_name"];
    self.addressTextField.text = [item valueForKey:@"address"];
    self.phoneTextField.text = [item valueForKey:@"phone"];
    self.emailTextField.text = [item valueForKey:@"email"];
    self.childNameTextField.text = [item valueForKey:@"child_name"];
    [self.genderButton setTitle:[item valueForKey:@"gender"] forState:UIControlStateNormal];
    NSDate *birthday = (NSDate *)[item valueForKey:@"birthday"];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:birthday];
    [self.dayButton setTitle:[NSString stringWithFormat:@"%d", components.day] forState:UIControlStateNormal];
    [self.monthButton setTitle:[NSString stringWithFormat:@"%d", components.month] forState:UIControlStateNormal];
    [self.yearButton setTitle:[NSString stringWithFormat:@"%d", components.year] forState:UIControlStateNormal];
    [self.productButton setTitle:[item valueForKey:@"current_product"] forState:UIControlStateNormal];
    [self.agreement1Button setSelected:[[item valueForKey:@"agreement1"] integerValue] == 1 ? YES : NO];
    [self.agreement2Button setSelected:[[item valueForKey:@"agreement2"] integerValue] == 1 ? YES : NO];
    [self.agreement3Button setSelected:[[item valueForKey:@"agreement3"] integerValue] == 1 ? YES : NO];
}

- (void) customerTapped:(NSDictionary *)item {
    [self showDetail:item];
}

@end
