//
//  CustomerItemViewController.m
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/12/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import "CustomerItemViewController.h"

@interface CustomerItemViewController ()

@end

@implementation CustomerItemViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.avatarImageView.image = [UIImage imageWithData:(NSData *)[self.customer valueForKey:@"picture"]];
    self.signatureImageView.image = [UIImage imageWithData:(NSData *)[self.customer valueForKey:@"signature"]];
    self.idLabel.text = [NSString stringWithFormat:@"%03d", [[self.customer valueForKey:@"id"] integerValue]];
    self.nameLabel.text = [self.customer valueForKey:@"mother_name"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    self.birthdayLabel.text = [dateFormatter stringFromDate:(NSDate *)[self.customer valueForKey:@"birthday"]];

    UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    tapped.delegate = self;
    [self.view addGestureRecognizer:tapped];
    tapped = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewTapped:(UITapGestureRecognizer *)gestureRecognizer {
    if (self.delegate) {
        [self.delegate customerTapped:self.customer];
    }
}

@end
