//
//  CustomerItemViewController.h
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/12/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomerItemTapDelegate

- (void)customerTapped:(NSDictionary *)item;

@end

@interface CustomerItemViewController: UIViewController<UIGestureRecognizerDelegate>

@property (strong, nonatomic) NSDictionary *customer;

@property (strong, nonatomic) id<CustomerItemTapDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (strong, nonatomic) IBOutlet UIImageView *signatureImageView;
@property (strong, nonatomic) IBOutlet UILabel *idLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *birthdayLabel;

@end
