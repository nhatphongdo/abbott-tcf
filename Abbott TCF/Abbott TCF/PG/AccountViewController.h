//
//  AccountViewController.h
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/10/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "CustomerItemViewController.h"


@interface AccountViewController : UIViewController<CustomerItemTapDelegate> {
    NSManagedObjectContext *managedObjectContext;
    NSArray *customers;
}

@property (strong, nonatomic) IBOutlet UILabel *accountLabel;
@property (strong, nonatomic) IBOutlet UILabel *countLabel;
@property (strong, nonatomic) IBOutlet UIButton *logoutButton;
@property (strong, nonatomic) IBOutlet UIScrollView *detailScrollView;
@property (strong, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (strong, nonatomic) IBOutlet UIImageView *signatureImageView;
@property (strong, nonatomic) IBOutlet UITextField *motherNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *addressTextField;
@property (strong, nonatomic) IBOutlet UITextField *phoneTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *childNameTextField;
@property (strong, nonatomic) IBOutlet UIButton *genderButton;
@property (strong, nonatomic) IBOutlet UIButton *dayButton;
@property (strong, nonatomic) IBOutlet UIButton *monthButton;
@property (strong, nonatomic) IBOutlet UIButton *yearButton;
@property (strong, nonatomic) IBOutlet UIButton *productButton;
@property (strong, nonatomic) IBOutlet UIButton *agreement1Button;
@property (strong, nonatomic) IBOutlet UIButton *agreement2Button;
@property (strong, nonatomic) IBOutlet UIButton *agreement3Button;
@property (strong, nonatomic) IBOutlet UIScrollView *listScrollView;
@property (strong, nonatomic) IBOutlet UIButton *backButton;

- (IBAction)logoutButtonTouched:(id)sender;
- (IBAction)backButtonTouched:(id)sender;
- (IBAction)updateButtonTouched:(id)sender;

@end
