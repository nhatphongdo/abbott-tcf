//
//  UseMilkQuestionViewController.m
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/23/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import "UseMilkQuestionViewController.h"

@interface UseMilkQuestionViewController ()

@end

@implementation UseMilkQuestionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
