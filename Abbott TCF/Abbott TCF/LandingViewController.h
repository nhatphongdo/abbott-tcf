//
//  HomeViewController.h
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/10/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LandingViewController : UIViewController<UIGestureRecognizerDelegate>{
    NSTimer * timer;
    float x1, x2, x3, x4, y1, y2, y3, y4, zoomValue;
    NSInteger  count1, count2, count3, count4;
    BOOL isShow;
}


@property (weak, nonatomic) IBOutlet UIImageView *imageOver;
@property (weak, nonatomic) IBOutlet UIImageView *imageTummyCare;
@property (weak, nonatomic) IBOutlet UIImageView *imageTcf;
@property (weak, nonatomic) IBOutlet UIImageView *imageIQ;
@property (weak, nonatomic) IBOutlet UIImageView *imageHeight;



- (IBAction)userButtonTouched:(id)sender;

@end
