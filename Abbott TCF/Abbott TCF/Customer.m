//
//  Customer.m
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import "Customer.h"


@implementation Customer

@dynamic id;
@dynamic mother_name;
@dynamic address;
@dynamic phone;
@dynamic email;
@dynamic child_name;
@dynamic gender;
@dynamic birthday;
@dynamic current_product;
@dynamic agreement1;
@dynamic agreement2;
@dynamic agreement3;
@dynamic picture;
@dynamic signature;
@dynamic question1;
@dynamic question2;
@dynamic question3;
@dynamic question4;
@dynamic question5;
@dynamic question6;

@end
