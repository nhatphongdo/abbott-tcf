//
//  HomeViewController.m
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/10/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import "LandingViewController.h"
#import "LoginViewController.h"

@interface LandingViewController ()

@end

@implementation LandingViewController

- (NSInteger)getY:(float)tx count:(NSInteger)count{
    
    float ty = [self getYByTan:tx*tx];
    ty = (count == 1? ty*(-1):ty);
    ty += [self getYMore:(tx - (-284)) tanValue:(0.176327)];
    
    return ty;
}

- (NSInteger)getYByTan:(float)tx{
    return sqrt((float)((float)(1 - (float)tx/(float)(284*284))) * 128*128);
}


- (void)roundAll{
//    NSLog(@"%f, %f", x1, y1);
    
    y1 = [self getY:(x1) count:count1];
    
    y2 =[self getY:(x2) count:count2];
    
    y3 = [self getY:(x3) count:count3];
    
    y4 = [self getY:(x4) count:count4];
    
//    y1 = (count1 == 1? y1*(-1):y1);
//    
//    y1 += [self getYMore:(x1 - (-284)) tanValue:(0.176327)];
    
    NSInteger yMax = [self getYByTan:0];
    //yMax = (count == 1? yMax*(-1):yMax);
    
    zoomValue = [self getYMore:(y1 - yMax) tanValue:0.194380];

    float z2,z3, z4;
    z2 = [self getYMore:(y2 - yMax)  tanValue:0.194380];
    z3 =[self getYMore:(y3 - yMax)  tanValue:0.194380];
    z4 = [self getYMore:(y4 - yMax)  tanValue:0.194380];
    
    self.imageIQ.frame = CGRectMake(x1 + 480 , y1 + 270, 100 + zoomValue, 100 + zoomValue);
    self.imageTummyCare.frame = CGRectMake(x2 + 480, y2 + 270, 100 + z2, 100 + z2);
    self.imageTcf.frame = CGRectMake(x3 + 480, y3 + 270, 100 + z3, 100 + z3);
    self.imageHeight.frame = CGRectMake(x4 + 480, y4 + 270, 100 + z4, 100 + z4);

    x1 += [self getStep:x1 count:count1];
    x2 += [self getStep:x2 count:count2];
    x3 += [self getStep:x3 count:count3];
    x4 += [self getStep:x4 count:count4];
    
    if(x1 > 284){
        x1 = 284;
        count1 = 1;
    }else if(x1 < -284){
        x1 = -284;
        count1 = 2;
    }
    
    if(x2 > 284){
        x2 = 284;
        count2 = 1;
    }else if(x2 < -284){
        x2 = -284;
        count2 = 2;
    }
    
    if(x3 > 284){
        x3 = 284;
        count3 = 1;
    }else if(x3 < -284){
        x3 = -284;
        count3 = 2;
    }
    
    if(x4 > 284){
        x4 = 284;
        count4 = 1;
    }else if(x4 < -284){
        x4 = -284;
        count4 = 2;
    }
    
}

- (float)getStep:(float)tx count:(NSInteger)count{
    if(count == 1){
        if(tx < -280 || tx > 280){
            return -0.2;
        }else{
            return  -1;
        }
    }else{
        if(tx > 280 || tx < -280){
            return 0.2;
        }else{
            return 1;
        }
    }
}

- (NSInteger)getYMore:(NSInteger)tx tanValue:(double)tanValue{
    //NSLog(@"%f, %f, %f", tanh(1), sinh(1), cosh(1));
    return tanValue * tx;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesEnded:touches withEvent:event];
    [self loginShow];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        x1 = 284;
        count1 = 1;
        
        x2 = 0;
        count2 = 1;
        
        x3 = 0;
        count3 = 2;
        
        x4 = -284;
        count4 = 2;
        
        timer = [NSTimer scheduledTimerWithTimeInterval: 0.005 target:self selector:@selector(roundAll) userInfo:nil repeats:YES];
        
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    isShow = NO;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)userButtonTouched:(id)sender {
    [self loginShow];
}

- (void)loginShow{
    if (!isShow) {
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        [self addChildViewController:viewController];
        [self.view addSubview:viewController.view];
        [viewController didMoveToParentViewController:self];
        isShow = YES;
    }

}

@end
