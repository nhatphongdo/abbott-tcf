//
//  LoginViewController.h
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/10/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController

- (IBAction)loginButtonTouched:(id)sender;

@end
