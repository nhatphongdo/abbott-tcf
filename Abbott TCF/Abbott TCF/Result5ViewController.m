//
//  Result5ViewController.m
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/22/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import "Result5ViewController.h"
#import "TummyViewController.h"

@interface Result5ViewController ()

@end

@implementation Result5ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButton:(id)sender {
    TummyViewController *tummyTestViewController = (TummyViewController *) self.parentViewController;
    [tummyTestViewController showResult4Screen];
    tummyTestViewController = nil;
}
@end
