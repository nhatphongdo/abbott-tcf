//
//  HomeViewController.h
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TutorViewController.h"

@interface HomeViewController : UIViewController<UIGestureRecognizerDelegate>{
    TutorViewController *tutorView;
}


@property (weak, nonatomic) IBOutlet UIImageView *childrendImage;

- (IBAction)testButtonTouched:(id)sender;
- (IBAction)knowledgeButtonTouched:(id)sender;
- (IBAction)infoButtonTouched:(id)sender;
- (IBAction)settingsButtonTouched:(id)sender;
- (IBAction)userButtonTouched:(id)sender;

@end
