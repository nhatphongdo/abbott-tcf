//
//  Result2ViewController.h
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/22/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Result2ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imageAvatar;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *resultMsg;
@property (weak, nonatomic) IBOutlet UIImageView *icon1Image;
@property (weak, nonatomic) IBOutlet UIImageView *icon2Image;
@property (weak, nonatomic) IBOutlet UIImageView *icon3Image;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

- (IBAction)backButton:(id)sender;
- (IBAction)nextButton:(id)sender;


@end
