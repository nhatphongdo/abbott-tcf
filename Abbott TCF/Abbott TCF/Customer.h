//
//  Customer.h
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Customer : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * mother_name;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * child_name;
@property (nonatomic, retain) NSString * gender;
@property (nonatomic, retain) NSDate * birthday;
@property (nonatomic, retain) NSString * current_product;
@property (nonatomic, retain) NSNumber * agreement1;
@property (nonatomic, retain) NSNumber * agreement2;
@property (nonatomic, retain) NSNumber * agreement3;
@property (nonatomic, retain) NSData * picture;
@property (nonatomic, retain) NSData * signature;
@property (nonatomic, retain) NSNumber * question1;
@property (nonatomic, retain) NSString * question2;
@property (nonatomic, retain) NSNumber * question3;
@property (nonatomic, retain) NSNumber * question4;
@property (nonatomic, retain) NSNumber * question5;
@property (nonatomic, retain) NSNumber * question6;

@end
