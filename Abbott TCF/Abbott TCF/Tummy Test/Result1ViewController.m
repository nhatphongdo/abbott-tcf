//
//  Result1ViewController.m
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/21/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import "Result1ViewController.h"
#import "TummyViewController.h"

@interface Result1ViewController ()

@end

@implementation Result1ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)nextButton:(id)sender {
    TummyViewController *tummyTestViewController = (TummyViewController *) self.parentViewController;
    [tummyTestViewController showResult2Screen];
    tummyTestViewController = nil;
}
@end
