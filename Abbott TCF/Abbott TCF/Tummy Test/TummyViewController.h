//
//  TummyViewController.h
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TutorViewController.h"
@interface TummyViewController : UIViewController{
    TutorViewController *tutorView;
    
}

@property (strong, nonatomic) IBOutlet UIView *rollView;
@property (strong, nonatomic) IBOutlet UIImageView *rollImageView;
@property (strong, nonatomic) IBOutlet UIImageView *rollResultImage;
@property (strong, nonatomic) IBOutlet UIImageView *rollTestImage;
@property (strong, nonatomic) IBOutlet UIImageView *rollRegisterImage;
@property (strong, nonatomic) IBOutlet UIImageView *rollTummyCareImage;

- (void) showRegistrationScreen;
- (void) showTummyTestScreen;
- (void) showTummyTestScreen2;
- (void) showTummyTestScreen3;
- (void) showResultScreen;
- (void) showResult1Screen;
- (void) showResult2Screen;
- (void) showResult3Screen;
- (void) showResult4Screen;
- (void) showResult5Screen;


- (IBAction)settingsButtonTouched:(id)sender;
- (IBAction)userButtonTouched:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *printButton;
@property (weak, nonatomic) IBOutlet UIButton *emailButton;

@end
