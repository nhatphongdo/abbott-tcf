//
//  TummyTest2ViewController.m
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/17/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import "TummyTest2ViewController.h"
#import "TummyViewController.h"


@interface TummyTest2ViewController ()

@end

@implementation TummyTest2ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)timeButtonTouched:(id)sender {
    UIButton *button = (UIButton *)sender;
    [button setSelected:!button.isSelected];
    
    // Use tag to get value
    NSInteger value = button.tag;
}

- (IBAction)frequencyButtonTouched:(id)sender {
    UIButton *button = (UIButton *)sender;
    [button setSelected:!button.isSelected];
    
    // Use tag to get value
    NSInteger value = button.tag;
}

- (IBAction)nextButtonTouched:(id)sender {
    TummyViewController *tummyTestViewController = (TummyViewController *) self.parentViewController;
    [tummyTestViewController showTummyTestScreen3];
    tummyTestViewController = nil;
}

@end
