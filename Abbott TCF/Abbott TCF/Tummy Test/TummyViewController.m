//
//  TummyViewController.m
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import "TummyViewController.h"
#import "RegisterViewController.h"
#import "AccountViewController.h"
#import "TummyTest1ViewController.h"
#import "TummyTest2ViewController.h"
#import "TummyTest3ViewController.h"

#import "Result1ViewController.h"
#import "Result2ViewController.h"
#import "Result3ViewController.h"
#import "Result4ViewController.h"
#import "Result5ViewController.h"

@interface TummyViewController ()

@end

@implementation TummyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    tutorView = [[TutorViewController alloc] initWithNibName:@"TutorViewController" bundle:nil];
    [self addChildViewController:tutorView];
    [self.view addSubview:tutorView.view];
    [tutorView didMoveToParentViewController:self];
    // Do any additional setup after loading the view from its nib.
}

-(void)showTutor{
    [tutorView.view setHidden:NO];
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationCurveEaseOut
                     animations:^{
                         [tutorView.view setAlpha:1];
                        }
                     completion:^(BOOL finish){
                         [tutorView.view setHidden:NO];
                        }];
}

-(void)hideTutor{
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationCurveEaseOut
                     animations:^{
                         [tutorView.view setAlpha:0];
                     }
                     completion:^(BOOL finish){
                         [tutorView.view setHidden:YES];
                     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) showRegistrationScreen {
    // Remove child view controllers
    for (UIViewController *child in self.childViewControllers) {
        [child didMoveToParentViewController:nil];
        [child.view removeFromSuperview];
        [child removeFromParentViewController];
    }
    
    // Add registration screen
    RegisterViewController *viewController = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    [self addChildViewController:viewController];
    [self.view addSubview:viewController.view];
    [viewController didMoveToParentViewController:self];
    
    viewController = nil;
}

- (void) showTummyTestScreen {
    // Remove child view controllers
    for (UIViewController *child in self.childViewControllers) {
        [child didMoveToParentViewController:nil];
        [child.view removeFromSuperview];
        [child removeFromParentViewController];
    }
    
    // Hide email and print button
    [self.printButton setHidden:NO];
    [self.emailButton setHidden:NO];
    
    // Add Tummy Test screen
    TummyTest1ViewController *viewController = [[TummyTest1ViewController alloc] initWithNibName:@"TummyTest1ViewController" bundle:nil];
    [self addChildViewController:viewController];
    [self.view addSubview:viewController.view];
    [viewController didMoveToParentViewController:self];
    
    viewController = nil;
    
    [self rotateRollImage:M_PI/2];
}

- (void) showTummyTestScreen2 {
    // Remove child view controllers
    for (UIViewController *child in self.childViewControllers) {
        [child didMoveToParentViewController:nil];
        [child.view removeFromSuperview];
        [child removeFromParentViewController];
    }
    
    // Hide email and print button
    [self.printButton setHidden:NO];
    [self.emailButton setHidden:NO];
    
    // Add Tummy Test screen
    TummyTest2ViewController *viewController = [[TummyTest2ViewController alloc] initWithNibName:@"TummyTest2ViewController" bundle:nil];
    [self addChildViewController:viewController];
    [self.view addSubview:viewController.view];
    [viewController didMoveToParentViewController:self];
    
    viewController = nil;
}

- (void) showTummyTestScreen3 {
    // Remove child view controllers
    for (UIViewController *child in self.childViewControllers) {
        [child didMoveToParentViewController:nil];
        [child.view removeFromSuperview];
        [child removeFromParentViewController];
    }
    
    // Hide email and print button
    [self.printButton setHidden:NO];
    [self.emailButton setHidden:NO];
    
    // Add Tummy Test screen
    TummyTest3ViewController *viewController = [[TummyTest3ViewController alloc] initWithNibName:@"TummyTest3ViewController" bundle:nil];
    [self addChildViewController:viewController];
    [self.view addSubview:viewController.view];
    [viewController didMoveToParentViewController:self];
    
    viewController = nil;
    [self rotateRollImage:M_PI/2];
    
}

-(void) showResult1Screen{
    for(UIViewController *child in self.childViewControllers){
        [child didMoveToParentViewController:nil];
        [child.view removeFromSuperview];
        [child removeFromParentViewController];
    }
    
    // Add Tummy Test Result 1 screen
    Result1ViewController *result1 = [[Result1ViewController alloc] initWithNibName:@"Result1ViewController" bundle:nil];
    [self addChildViewController:result1];
    [self.view addSubview:result1.view];
    [result1 didMoveToParentViewController:self];
    result1 = nil;
    
    [self.emailButton setHidden:NO];
    [self.printButton setHidden:NO];
    [self rotateRollImage:M_PI/2];
    
}

-(void) showResult2Screen{
    for(UIViewController *child in self.childViewControllers){
        [child didMoveToParentViewController:nil];
        [child.view removeFromSuperview];
        [child removeFromParentViewController];
    }
    
    // Add Tummy Test Result 1 screen
    Result2ViewController *result1 = [[Result2ViewController alloc] initWithNibName:@"Result2ViewController" bundle:nil];
    [self addChildViewController:result1];
    [self.view addSubview:result1.view];
    [result1 didMoveToParentViewController:self];
    result1 = nil;
    
    [self.emailButton setHidden:NO];
    [self.printButton setHidden:NO];
    [self rotateRollImage:M_PI/2];
    
}

-(void) showResult3Screen{
    for(UIViewController *child in self.childViewControllers){
        [child didMoveToParentViewController:nil];
        [child.view removeFromSuperview];
        [child removeFromParentViewController];
    }
    
    // Add Tummy Test Result 1 screen
    Result3ViewController *result3 = [[Result3ViewController alloc] initWithNibName:@"Result3ViewController" bundle:nil];
    [self addChildViewController:result3];
    [self.view addSubview:result3.view];
    [result3 didMoveToParentViewController:self];
    result3 = nil;
    
    [self.emailButton setHidden:NO];
    [self.printButton setHidden:NO];
    [self rotateRollImage:M_PI/2];
    
}

-(void) showResult4Screen{
    for(UIViewController *child in self.childViewControllers){
        [child didMoveToParentViewController:nil];
        [child.view removeFromSuperview];
        [child removeFromParentViewController];
    }
    
    // Add Tummy Test Result 1 screen
    Result4ViewController *result4 = [[Result4ViewController alloc] initWithNibName:@"Result4ViewController" bundle:nil];
    [self addChildViewController:result4];
    [self.view addSubview:result4.view];
    [result4 didMoveToParentViewController:self];
    result4 = nil;
    
    [self.emailButton setHidden:NO];
    [self.printButton setHidden:NO];
    [self rotateRollImage:M_PI/2];
    
}

-(void) showResult5Screen{
    for(UIViewController *child in self.childViewControllers){
        [child didMoveToParentViewController:nil];
        [child.view removeFromSuperview];
        [child removeFromParentViewController];
    }
    
    // Add Tummy Test Result 1 screen
    Result5ViewController *result5 = [[Result5ViewController alloc] initWithNibName:@"Result5ViewController" bundle:nil];
    [self addChildViewController:result5];
    [self.view addSubview:result5.view];
    [result5 didMoveToParentViewController:self];
    result5 = nil;
    
    [self.emailButton setHidden:NO];
    [self.printButton setHidden:NO];
    [self rotateRollImage:M_PI/2];
    
}

- (void) showResultScreen {
    
}

- (void) rotateRollImage:(float)angle {
    [UIView animateWithDuration:2
                          delay:0.0
                        options:UIViewAnimationCurveEaseOut
                     animations:^{
                         self.rollImageView.transform = CGAffineTransformMakeRotation(angle);

//                         float radius = 50;
//                         
//                         float curAngle = atan((self.rollTestImage.frame.origin.y + self.rollTestImage.frame.size.height / 2 - self.rollView.frame.size.height / 2) / (self.rollTestImage.frame.origin.x + self.rollTestImage.frame.size.width / 2 - self.rollView.frame.size.width / 2));
//                         float x = tan(curAngle + angle) * radius + self.rollView.frame.size.width / 2;
//                         float y = tan(curAngle + angle) * radius + self.rollView.frame.size.height / 2;
//                         self.rollTestImage.frame = CGRectMake(x - self.rollTestImage.frame.size.width / 2, y - self.rollTestImage.frame.size.height / 2, self.rollTestImage.frame.size.width, self.rollTestImage.frame.size.height);
//
//                         
//                         curAngle = atan((self.rollRegisterImage.frame.origin.y + self.rollRegisterImage.frame.size.height / 2 - self.rollView.frame.size.height / 2) / (self.rollRegisterImage.frame.origin.x + self.rollRegisterImage.frame.size.height / 2 - self.rollView.frame.size.width / 2));
//                         x = tan(curAngle + angle) * radius + self.rollView.frame.size.width / 2;
//                         y = tan(curAngle + angle) * radius + self.rollView.frame.size.height / 2;
//                         self.rollRegisterImage.frame = CGRectMake(x - self.rollRegisterImage.frame.size.width / 2, y - self.rollRegisterImage.frame.size.height / 2, self.rollRegisterImage.frame.size.width, self.rollRegisterImage.frame.size.height);
//
//                         curAngle = atan((self.rollResultImage.frame.origin.y + self.rollResultImage.frame.size.height / 2 - self.rollView.frame.size.height / 2) / (self.rollResultImage.frame.origin.x + self.rollResultImage.frame.size.height / 2 - self.rollView.frame.size.width / 2));
//                         x = tan(curAngle + angle) * radius + self.rollView.frame.size.width / 2;
//                         y = tan(curAngle + angle) * radius + self.rollView.frame.size.height / 2;
//                         self.rollResultImage.frame = CGRectMake(x - self.rollResultImage.frame.size.width / 2, y - self.rollResultImage.frame.size.height / 2, self.rollResultImage.frame.size.width, self.rollResultImage.frame.size.height);
//
//                         curAngle = atan((self.rollTummyCareImage.frame.origin.y + self.rollTummyCareImage.frame.size.height / 2 - self.rollView.frame.size.height / 2) / (self.rollTummyCareImage.frame.origin.x + self.rollTummyCareImage.frame.size.height / 2 - self.rollView.frame.size.width / 2));
//                         x = tan(curAngle + angle) * radius + self.rollView.frame.size.width / 2;
//                         y = tan(curAngle + angle) * radius + self.rollView.frame.size.height / 2;
//                         self.rollTummyCareImage.frame = CGRectMake(x - self.rollTummyCareImage.frame.size.width / 2, y - self.rollTummyCareImage.frame.size.height / 2, self.rollTummyCareImage.frame.size.width, self.rollTummyCareImage.frame.size.height);
                     }
                     completion:^(BOOL finished){
                     }];
    
}

- (IBAction)settingsButtonTouched:(id)sender {
    [self showTutor];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self hideTutor];
}

- (IBAction)userButtonTouched:(id)sender {
    AccountViewController *viewController = [[AccountViewController alloc] initWithNibName:@"AccountViewController" bundle:nil];
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
