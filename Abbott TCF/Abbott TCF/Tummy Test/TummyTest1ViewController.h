//
//  TummyTest1ViewController.h
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/17/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreData/CoreData.h>

@interface TummyTest1ViewController : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate> {
    NSArray *milkArray;
    
}
@property (weak, nonatomic) IBOutlet UIPickerView *milkPicker;
@property (weak, nonatomic) IBOutlet UIButton *milkButton;

- (IBAction)symptomButtonTouched:(id)sender;
- (IBAction)nextButtonTouched:(id)sender;

- (IBAction)milkButtonTouched:(id)sender;

@end
