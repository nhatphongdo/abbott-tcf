//
//  TummyTest3ViewController.m
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/17/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import "TummyTest3ViewController.h"
#import "TummyViewController.h"

@interface TummyTest3ViewController ()

@end

@implementation TummyTest3ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showResultButton:(id)sender {
    TummyViewController *tummyTestViewController = (TummyViewController *) self.parentViewController;
    [tummyTestViewController showResult1Screen];
    tummyTestViewController = nil;
}
@end
