//
//  Result1ViewController.h
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/21/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Result1ViewController : UIViewController
- (IBAction)nextButton:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UILabel *birthdayLable;
@property (weak, nonatomic) IBOutlet UIImageView *milkStandard;
@property (weak, nonatomic) IBOutlet UILabel *resultMsg1;
@property (weak, nonatomic) IBOutlet UILabel *resultMsg2;
@property (weak, nonatomic) IBOutlet UILabel *resultMsg3;

@end
