//
//  TummyTest3ViewController.h
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/17/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TummyTest3ViewController : UIViewController
- (IBAction)showResultButton:(id)sender;

@end
