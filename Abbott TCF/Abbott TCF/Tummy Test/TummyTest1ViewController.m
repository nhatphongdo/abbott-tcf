//
//  TummyTest1ViewController.m
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/17/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//


#import "TummyTest1ViewController.h"
#import "TummyViewController.h"


@interface TummyTest1ViewController ()

@end

@implementation TummyTest1ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //milkArray = [NSMutableArray alloc];
        /*for(int i = 60; i < 960; i += 30){
            [milkArray addObject:[NSDecimalNumber numberWithInt:i]];
        }*/
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.milkPicker setFrame:CGRectMake(self.milkPicker.frame.origin.x, self.milkPicker.frame.origin.y, self.milkPicker.frame.size.width, 162)];
    
    UITapGestureRecognizer *pickerTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickerViewTapGestureRecognized:)];
    
    pickerTapped.delegate = self;
    [self.milkPicker addGestureRecognizer:pickerTapped];
    pickerTapped = nil;
    
    //self.milkPicker.dataSource = self;
    //self.milkPicker.delegate = self;
    // Do any additional setup after loading the view from its nib.
}

- (void)pickerViewTapGestureRecognized:(UITapGestureRecognizer*)gestureRecognizer {
    CGPoint touchPoint = [gestureRecognizer locationInView:gestureRecognizer.view.superview];
    
    CGRect frame = gestureRecognizer.view.frame;
    CGRect selectorFrame = CGRectInset(frame, 0.0, gestureRecognizer.view.bounds.size.height * 0.85 / 2.0 );
    
    if (CGRectContainsPoint(selectorFrame, touchPoint)) {
        UIPickerView *thePickerView = (UIPickerView *) gestureRecognizer.view;
        NSInteger row = [thePickerView selectedRowInComponent:0];
        
        if (thePickerView == self.milkPicker){
            [self.milkButton setTitle:[milkArray objectAtIndex:row] forState:UIControlStateNormal];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)symptomButtonTouched:(id)sender {
    UIButton *button = (UIButton *)sender;
    [button setSelected:!button.isSelected];
    
    // Use tag to get value
    NSInteger value = button.tag;
}

- (IBAction)nextButtonTouched:(id)sender {
    TummyViewController *tummyTestViewController = (TummyViewController *) self.parentViewController;
    [tummyTestViewController showTummyTestScreen2];
    tummyTestViewController = nil;
}

- (IBAction)milkButtonTouched:(id)sender {
    [self.milkPicker setHidden:NO];
}


@end
