//
//  RegisterViewController.m
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import "RegisterViewController.h"
#import "AppDelegate.h"


@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        gendersArray = [NSArray arrayWithObjects:
                        @"Nam",
                        @"Nữ",
                        nil];
        productsArray = [NSArray arrayWithObjects:
                         @"Grow G-Power",
                         @"Pediasure",
                         @"GainPlus Total Comfort",
                         @"Dumex DuGrow Gold",
                         @"Dutch Lady Complete",
                         @"Dutch Lady 123",
                         @"Friso Gold",
                         @"Enfalac A+",
                         @"EnfaGrow A+",
                         @"Pregestimil",
                         @"Prosobee",
                         @"Nestle Gau",
                         @"NAN Pro 3",
                         @"Lactogen Gold 3",
                         @"Grow Plus",
                         @"Nuti Pedia",
                         @"Pediaplus Lysine",
                         @"Dielac Optimum",
                         @"Vinamilk",
                         @"Dielac Alpha",
                         nil];
        yearsArray = [NSArray arrayWithObjects:
                      @"2000",
                      @"2001",
                      @"2002",
                      @"2003",
                      @"2004",
                      @"2005",
                      @"2006",
                      @"2007",
                      @"2008",
                      @"2009",
                      @"2010",
                      @"2011",
                      @"2012",
                      @"2013",
                      @"2014",
                      nil];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.genderPicker setFrame:CGRectMake(self.genderPicker.frame.origin.x, self.genderPicker.frame.origin.y, self.genderPicker.frame.size.width, 162)];
    [self.dayPicker setFrame:CGRectMake(self.dayPicker.frame.origin.x, self.dayPicker.frame.origin.y, self.dayPicker.frame.size.width, 162)];
    [self.monthPicker setFrame:CGRectMake(self.monthPicker.frame.origin.x, self.monthPicker.frame.origin.y, self.monthPicker.frame.size.width, 162)];
    [self.yearPicker setFrame:CGRectMake(self.yearPicker.frame.origin.x, self.yearPicker.frame.origin.y, self.yearPicker.frame.size.width, 162)];
    [self.productPicker setFrame:CGRectMake(self.productPicker.frame.origin.x, self.productPicker.frame.origin.y, self.productPicker.frame.size.width, 162)];
    
    UITapGestureRecognizer *pickerTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickerViewTapGestureRecognized:)];
    pickerTapped.delegate = self;
    [self.genderPicker addGestureRecognizer:pickerTapped];
    pickerTapped = nil;
    pickerTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickerViewTapGestureRecognized:)];
    pickerTapped.delegate = self;
    [self.dayPicker addGestureRecognizer:pickerTapped];
    pickerTapped = nil;
    pickerTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickerViewTapGestureRecognized:)];
    pickerTapped.delegate = self;
    [self.monthPicker addGestureRecognizer:pickerTapped];
    pickerTapped = nil;
    pickerTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickerViewTapGestureRecognized:)];
    pickerTapped.delegate = self;
    [self.yearPicker addGestureRecognizer:pickerTapped];
    pickerTapped = nil;
    pickerTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickerViewTapGestureRecognized:)];
    pickerTapped.delegate = self;
    [self.productPicker addGestureRecognizer:pickerTapped];
    pickerTapped = nil;
    
    self.genderPicker.dataSource = self;
    self.genderPicker.delegate = self;
    self.dayPicker.dataSource = self;
    self.dayPicker.delegate = self;
    self.monthPicker.dataSource = self;
    self.monthPicker.delegate = self;
    self.yearPicker.dataSource = self;
    self.yearPicker.delegate = self;
    self.productPicker.dataSource = self;
    self.productPicker.delegate = self;
    
    self.motherNameTextField.delegate = self;
    self.addressTextField.delegate = self;
    self.phoneTextField.delegate = self;
    self.emailTextField.delegate = self;
    self.childNameTextField.delegate = self;
    
    
    [self.agreement1Button setSelected:YES];
    [self.agreement2Button setSelected:YES];
    [self.agreement3Button setSelected:YES];
    
    NSDictionary *underlineAttribute = @{NSForegroundColorAttributeName: [UIColor colorWithRed:185/255.0 green:25/255.0 blue:139/255.0 alpha:1.0],
                                         NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),
                                         NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-LightItalic" size:10.0]};
    self.unsubscribeButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"nhấn vào đây" attributes:underlineAttribute];
    
    // Setup signature pad
    signatureController = [[SignatureViewController alloc] initWithNibName:@"SignatureView" bundle:nil];
    signatureController.delegate = self;
    signatureController.view.frame = CGRectMake(0, 0, self.signatureView.frame.size.width, self.signatureView.frame.size.height);
    [self.signatureView addSubview:signatureController.view];
    
    // Fetch data
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Customer"inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setResultType:NSDictionaryResultType];
    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObject:@"id"]];
    NSError *error;
    customers = [NSMutableArray arrayWithArray:[managedObjectContext executeFetchRequest:fetchRequest error:&error]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    if (previewLayer != nil) {
        if ([self interfaceOrientation] == UIInterfaceOrientationLandscapeLeft) {
            [[previewLayer connection] setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
        }
        else {
            [[previewLayer connection] setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
        }
    }
}

- (IBAction)genderButtonTouched:(id)sender {
    [self.genderPicker setHidden:NO];
    [self.dayPicker setHidden:YES];
    [self.monthPicker setHidden:YES];
    [self.yearPicker setHidden:YES];
    [self.productPicker setHidden:YES];
}

- (IBAction)dayButtonTouched:(id)sender {
    [self.dayPicker setHidden:NO];
    [self.genderPicker setHidden:YES];
    [self.monthPicker setHidden:YES];
    [self.yearPicker setHidden:YES];
    [self.productPicker setHidden:YES];
}

- (IBAction)monthButtonTouched:(id)sender {
    [self.monthPicker setHidden:NO];
    [self.dayPicker setHidden:YES];
    [self.genderPicker setHidden:YES];
    [self.yearPicker setHidden:YES];
    [self.productPicker setHidden:YES];
}

- (IBAction)yearButtonTouched:(id)sender {
    [self.yearPicker setHidden:NO];
    [self.dayPicker setHidden:YES];
    [self.monthPicker setHidden:YES];
    [self.genderPicker setHidden:YES];
    [self.productPicker setHidden:YES];
}

- (IBAction)productButtonTouched:(id)sender {
    [self.productPicker setHidden:NO];
    [self.dayPicker setHidden:YES];
    [self.monthPicker setHidden:YES];
    [self.yearPicker setHidden:YES];
    [self.genderPicker setHidden:YES];
}

- (IBAction)agreement1ButtonTouched:(id)sender {
    [self.agreement1Button setSelected:!self.agreement1Button.isSelected];
}

- (IBAction)agreement2ButtonTouched:(id)sender {
    [self.agreement2Button setSelected:!self.agreement2Button.isSelected];
}

- (IBAction)agreement3ButtonTouched:(id)sender {
    [self.agreement3Button setSelected:!self.agreement3Button.isSelected];
}

- (IBAction)addImageButtonTouched:(id)sender {
    session = [[AVCaptureSession alloc] init];
    session.sessionPreset = AVCaptureSessionPresetMedium;
    
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    NSError *error = nil;
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    if (!input) {
        // Handle the error appropriately.
    }
    [session addInput:input];

    stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil];
    [stillImageOutput setOutputSettings:outputSettings];
    [session addOutput:stillImageOutput];
    
    previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:session];
    //Set the preview layer frame
    previewLayer.frame = self.cameraView.bounds;
    [previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    if ([self interfaceOrientation] == UIInterfaceOrientationLandscapeLeft) {
        [[previewLayer connection] setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
    }
    else {
        [[previewLayer connection] setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
    }
    //Now you can add this layer to a view of your view controller
    [self.cameraView.layer addSublayer:previewLayer];
    [self.cameraView setHidden:NO];
    [self.captureButton setHidden:NO];
    [session startRunning];
}

- (IBAction)signatureButtonTouched:(id)sender {
    [self.signatureView setHidden:NO];
}

- (IBAction)unsubscribeButtonTouched:(id)sender {
}

- (IBAction)submitButtonTouched:(id)sender {
    if ([self.motherNameTextField.text length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Abbott" message:@"Vui lòng nhập Tên Của Mẹ" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [self.motherNameTextField becomeFirstResponder];
        return;
    }
    if ([self.addressTextField.text length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Abbott" message:@"Vui lòng nhập Địa chỉ" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [self.addressTextField becomeFirstResponder];
        return;
    }
    if ([self.phoneTextField.text length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Abbott" message:@"Vui lòng nhập Số điện thoại" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [self.phoneTextField becomeFirstResponder];
        return;
    }
    if ([self.emailTextField.text length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Abbott" message:@"Vui lòng nhập Địa chỉ Email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [self.emailTextField becomeFirstResponder];
        return;
    }
    if ([self.childNameTextField.text length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Abbott" message:@"Vui lòng nhập Tên của bé" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [self.childNameTextField becomeFirstResponder];
        return;
    }
    if ([self.genderButton.titleLabel.text length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Abbott" message:@"Vui lòng chọn Giới tính của bé" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    if ([self.dayButton.titleLabel.text length] == 0 || [self.monthButton.titleLabel.text length] == 0 || [self.yearButton.titleLabel.text length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Abbott" message:@"Vui lòng chọn Ngày tháng năm sinh của bé" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    if ([self.productButton.titleLabel.text length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Abbott" message:@"Vui lòng chọn Sản phẩm bé đang dùng" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    if (self.agreement1Button.isSelected == NO || self.agreement2Button.isSelected == NO || self.agreement3Button.isSelected == NO) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Abbott" message:@"Vui lòng đồng ý với Thể lệ và điều khoản của chương trình" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    
    NSManagedObjectContext *context = managedObjectContext;
    NSManagedObject *customerInfo = [NSEntityDescription insertNewObjectForEntityForName:@"Customer" inManagedObjectContext:context];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"d/M/yyyy"];
    NSDate *birthday = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@/%@/%@", self.dayButton.titleLabel.text, self.monthButton.titleLabel.text, self.yearButton.titleLabel.text]];

    NSInteger newID = 0;
    for (NSDictionary *dict in customers) {
        NSInteger IDToCompare = [[dict valueForKey:@"id"] integerValue];
        
        if (IDToCompare >= newID) {
            newID = IDToCompare + 1;
        }
    }
    
    if (newID == 0) {
        newID = 1;
    }

    [customerInfo setValue:[NSNumber numberWithInteger:newID] forKey:@"id"];
    [customerInfo setValue:self.motherNameTextField.text forKey:@"mother_name"];
    [customerInfo setValue:self.addressTextField.text forKey:@"address"];
    [customerInfo setValue:self.phoneTextField.text forKey:@"phone"];
    [customerInfo setValue:self.emailTextField.text forKey:@"email"];
    [customerInfo setValue:self.childNameTextField.text forKey:@"child_name"];
    [customerInfo setValue:self.genderButton.titleLabel.text forKey:@"gender"];
    [customerInfo setValue:birthday forKey:@"birthday"];
    [customerInfo setValue:self.productButton.titleLabel.text forKey:@"current_product"];
    [customerInfo setValue:[NSNumber numberWithBool:self.agreement1Button.isSelected] forKey:@"agreement1"];
    [customerInfo setValue:[NSNumber numberWithBool:self.agreement2Button.isSelected] forKey:@"agreement2"];
    [customerInfo setValue:[NSNumber numberWithBool:self.agreement3Button.isSelected] forKey:@"agreement3"];
    [customerInfo setValue:UIImagePNGRepresentation(self.avatarImageView.image) forKey:@"picture"];
    [customerInfo setValue:UIImagePNGRepresentation(self.signatureImageView.image) forKey:@"signature"];

    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    else {
        [customers addObject:customerInfo];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Abbott" message:@"Thông tin khách hàng đã được đăng ký thành công!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)captureButtonTouched:(id)sender {
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in stillImageOutput.connections)
    {
        for (AVCaptureInputPort *port in [connection inputPorts])
        {
            if ([[port mediaType] isEqual:AVMediaTypeVideo] )
            {
                videoConnection = connection;
                break;
            }
        }
        if (videoConnection) { break; }
    }
    
    if ([self interfaceOrientation] == UIInterfaceOrientationLandscapeLeft) {
        [videoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
    }
    else {
        [videoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
    }
    
    [stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error) {
        if (imageSampleBuffer != NULL) {
            NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
            UIImage *image = [[UIImage alloc] initWithData:imageData];
            [self.avatarImageView setImage:image];
            image = nil;
            imageData = nil;
        }

        if (session != nil) {
            [session stopRunning];
        }
    }];
    
    [self.cameraView setHidden:YES];
    [self.captureButton setHidden:YES];
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    if (thePickerView == self.genderPicker){
        return [gendersArray count];
    }
    else if (thePickerView == self.dayPicker) {
        return 31;
    }
    else if (thePickerView == self.monthPicker) {
        return 12;
    }
    else if (thePickerView == self.yearPicker) {
        return [yearsArray count];
    }
    else if (thePickerView == self.productPicker) {
        return [productsArray count];
    }
    else {
        return 1;
    }
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (thePickerView == self.genderPicker){
        return [gendersArray objectAtIndex:row];
    }
    else if (thePickerView == self.dayPicker) {
        return [NSString stringWithFormat:@"%d", row + 1];
    }
    else if (thePickerView == self.monthPicker) {
        return [NSString stringWithFormat:@"%d", row + 1];
    }
    else if (thePickerView == self.yearPicker) {
        return [yearsArray objectAtIndex:row];
    }
    else if (thePickerView == self.productPicker) {
        return [productsArray objectAtIndex:row];
    }
    else {
        return @"";
    }
}

- (void)pickerViewTapGestureRecognized:(UITapGestureRecognizer*)gestureRecognizer {
    CGPoint touchPoint = [gestureRecognizer locationInView:gestureRecognizer.view.superview];
    
    CGRect frame = gestureRecognizer.view.frame;
    CGRect selectorFrame = CGRectInset(frame, 0.0, gestureRecognizer.view.bounds.size.height * 0.85 / 2.0 );
    
    if (CGRectContainsPoint(selectorFrame, touchPoint)) {
        UIPickerView *thePickerView = (UIPickerView *) gestureRecognizer.view;
        NSInteger row = [thePickerView selectedRowInComponent:0];
        
        if (thePickerView == self.genderPicker){
            [self.genderButton setTitle:[gendersArray objectAtIndex:row] forState:UIControlStateNormal];
        }
        else if (thePickerView == self.dayPicker) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"d/M/yyyy"];
            NSDate *birthday = [dateFormatter dateFromString:[NSString stringWithFormat:@"%d/%@/%@", row + 1, self.monthButton.titleLabel.text, self.yearButton.titleLabel.text]];
            if (self.monthButton.titleLabel.text.length == 0 || self.yearButton.titleLabel.text.length == 0 || birthday != nil) {
                [self.dayButton setTitle:[NSString stringWithFormat:@"%d", row + 1] forState:UIControlStateNormal];
            }
            else {
                return;
            }
        }
        else if (thePickerView == self.monthPicker) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"d/M/yyyy"];
            NSDate *birthday = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@/%d/%@", self.dayButton.titleLabel.text, row + 1, self.yearButton.titleLabel.text]];
            if (self.dayButton.titleLabel.text.length == 0 || self.yearButton.titleLabel.text.length == 0 || birthday != nil) {
                [self.monthButton setTitle:[NSString stringWithFormat:@"%d", row + 1] forState:UIControlStateNormal];
            }
            else {
                return;
            }
        }
        else if (thePickerView == self.yearPicker) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"d/M/yyyy"];
            NSDate *birthday = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@/%@/%@", self.dayButton.titleLabel.text, self.monthButton.titleLabel.text, [yearsArray objectAtIndex:row]]];
            if (self.monthButton.titleLabel.text.length == 0 || self.dayButton.titleLabel.text.length == 0 || birthday != nil) {
                [self.yearButton setTitle:[yearsArray objectAtIndex:row] forState:UIControlStateNormal];
            }
            else {
                return;
            }
        }
        else if (thePickerView == self.productPicker) {
            [self.productButton setTitle:[productsArray objectAtIndex:row] forState:UIControlStateNormal];
        }
        
        [thePickerView setHidden:YES];
        
        thePickerView = nil;
    }
}


#pragma mark SignatureViewController delegate methods
- (void) signatureViewController:(SignatureViewController *)viewController didSign:(NSData *)signatureData {
    UIImage *image = [UIImage imageWithData:signatureData];
    [self.signatureImageView setImage:image];
    image = nil;
    
    [self.signatureView setHidden:YES];
}


- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.motherNameTextField) {
        [self.addressTextField becomeFirstResponder];
    }
    else if (textField == self.addressTextField) {
        [self.phoneTextField becomeFirstResponder];
    }
    else if (textField == self.phoneTextField) {
        [self.emailTextField becomeFirstResponder];
    }
    else if (textField == self.emailTextField) {
        [self.childNameTextField becomeFirstResponder];
    }
    else if (textField == self.childNameTextField) {
        [self.view endEditing:YES];
    }
    
    return YES;
}

@end
