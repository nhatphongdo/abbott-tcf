//
//  RegisterViewController.h
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "SignatureViewController.h"
#import <CoreData/CoreData.h>


@interface RegisterViewController : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, SignatureViewControllerDelegate> {
    NSArray *gendersArray;
    NSArray *yearsArray;
    NSArray *productsArray;
    
    AVCaptureSession *session;
    AVCaptureVideoPreviewLayer *previewLayer;
    AVCaptureStillImageOutput *stillImageOutput;
    
    SignatureViewController *signatureController;
    
    NSManagedObjectContext *managedObjectContext;
    NSMutableArray *customers;
}

@property (strong, nonatomic) IBOutlet UITextField *motherNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *addressTextField;
@property (strong, nonatomic) IBOutlet UITextField *phoneTextField;
@property (strong, nonatomic) IBOutlet UITextField *childNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;

@property (strong, nonatomic) IBOutlet UIButton *genderButton;
@property (strong, nonatomic) IBOutlet UIButton *dayButton;
@property (strong, nonatomic) IBOutlet UIButton *monthButton;
@property (strong, nonatomic) IBOutlet UIButton *yearButton;
@property (strong, nonatomic) IBOutlet UIButton *productButton;
@property (strong, nonatomic) IBOutlet UIButton *agreement1Button;
@property (strong, nonatomic) IBOutlet UIButton *agreement2Button;
@property (strong, nonatomic) IBOutlet UIButton *agreement3Button;
@property (strong, nonatomic) IBOutlet UIPickerView *genderPicker;
@property (strong, nonatomic) IBOutlet UIPickerView *dayPicker;
@property (strong, nonatomic) IBOutlet UIPickerView *monthPicker;
@property (strong, nonatomic) IBOutlet UIPickerView *yearPicker;
@property (strong, nonatomic) IBOutlet UIPickerView *productPicker;
@property (strong, nonatomic) IBOutlet UIButton *addImageButton;
@property (strong, nonatomic) IBOutlet UIButton *signatureButton;
@property (strong, nonatomic) IBOutlet UIButton *unsubscribeButton;
@property (strong, nonatomic) IBOutlet UIView *cameraView;
@property (strong, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (strong, nonatomic) IBOutlet UIImageView *signatureImageView;
@property (strong, nonatomic) IBOutlet UIButton *captureButton;
@property (strong, nonatomic) IBOutlet UIView *signatureView;

- (IBAction)genderButtonTouched:(id)sender;
- (IBAction)dayButtonTouched:(id)sender;
- (IBAction)monthButtonTouched:(id)sender;
- (IBAction)yearButtonTouched:(id)sender;
- (IBAction)productButtonTouched:(id)sender;
- (IBAction)agreement1ButtonTouched:(id)sender;
- (IBAction)agreement2ButtonTouched:(id)sender;
- (IBAction)agreement3ButtonTouched:(id)sender;
- (IBAction)addImageButtonTouched:(id)sender;
- (IBAction)signatureButtonTouched:(id)sender;
- (IBAction)unsubscribeButtonTouched:(id)sender;
- (IBAction)submitButtonTouched:(id)sender;
- (IBAction)captureButtonTouched:(id)sender;

@end
