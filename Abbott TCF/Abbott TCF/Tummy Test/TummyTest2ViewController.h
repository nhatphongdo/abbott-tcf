//
//  TummyTest2ViewController.h
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/17/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TummyTest2ViewController : UIViewController

- (IBAction)timeButtonTouched:(id)sender;
- (IBAction)frequencyButtonTouched:(id)sender;
- (IBAction)nextButtonTouched:(id)sender;

@end
