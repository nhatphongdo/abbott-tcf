//
//  Result3ViewController.h
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/22/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Result3ViewController : UIViewController
- (IBAction)backButton:(id)sender;
- (IBAction)nextButton:(id)sender;

@end
