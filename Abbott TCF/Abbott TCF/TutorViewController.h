//
//  TutorViewController.h
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/18/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorViewController : UIViewController<UIGestureRecognizerDelegate>

@end
