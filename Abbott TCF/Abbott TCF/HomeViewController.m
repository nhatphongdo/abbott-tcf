//
//  HomeViewController.m
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import "HomeViewController.h"
#import "KnowledgeViewController.h"
#import "TummyViewController.h"
#import "AccountViewController.h"


@interface HomeViewController ()

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    
    return self;
}

- (void)bounce1AnimationStopped {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.9/1];
    
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(bounce2AnimationStopped)];
    self.childrendImage.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
    [UIView commitAnimations];
}

- (void)bounce2AnimationStopped {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:4/2];
    self.childrendImage.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(showOver)];
    [UIView commitAnimations];
}

- (void)showOver{
    /*[self.childrendImage setAlpha:0.4];
    [self.guideImageView setAlpha:1.0];
    [self.guideImageView setHidden:NO];*/
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationCurveEaseOut
                     animations:^{
                         
                         [tutorView.view setAlpha:1.0];
                         [self.childrendImage setAlpha:0.4];
                     }
                     completion:^(BOOL finished){
                         [tutorView.view setHidden:NO];
                         
                     }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    tutorView = [[TutorViewController alloc] initWithNibName:@"TutorViewController" bundle:nil];
    [self addChildViewController:tutorView];
    [self.view addSubview:tutorView.view];
    //[self.view insertSubview:tutorView.view aboveSubview:self.view];
    [tutorView didMoveToParentViewController:self];
    
    
    // Do any additional setup after loading the view from its nib.
    self.childrendImage.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3/1.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(bounce1AnimationStopped)];
    [self.view addSubview:self.childrendImage];
    self.childrendImage.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
    
    [UIView commitAnimations];
    
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(guideTapped:)];
    tapGesture.delegate = self;
    [tutorView.view addGestureRecognizer:tapGesture];
    tapGesture = nil;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)testButtonTouched:(id)sender {
    TummyViewController *viewController = [[TummyViewController alloc] initWithNibName:@"TummyViewController" bundle:nil];
    [self.navigationController pushViewController:viewController animated:YES];
//    [viewController showRegistrationScreen];
    [viewController showTummyTestScreen];
    viewController = nil;
}

- (IBAction)knowledgeButtonTouched:(id)sender {
    KnowledgeViewController *viewController = [[KnowledgeViewController alloc] initWithNibName:@"KnowledgeViewController" bundle:nil];
    [self.navigationController pushViewController:viewController animated:YES];
    viewController = nil;
}

- (IBAction)infoButtonTouched:(id)sender {
}

- (IBAction)settingsButtonTouched:(id)sender {
    [self showOver];
}

- (IBAction)userButtonTouched:(id)sender {
    AccountViewController *viewController = [[AccountViewController alloc] initWithNibName:@"AccountViewController" bundle:nil];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void) guideTapped:(UITapGestureRecognizer *)gestureRecognizer {
    // Dismiss guide view
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationCurveEaseOut
                     animations:^{
                         
                         [tutorView.view setAlpha:0.0];
                         [self.childrendImage setAlpha:1];
                     }
                     completion:^(BOOL finished){
                         [tutorView.view setHidden:YES];
                         
                     }];
}

@end
