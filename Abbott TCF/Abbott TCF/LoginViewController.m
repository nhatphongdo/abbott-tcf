//
//  LoginViewController.m
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/10/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import "LoginViewController.h"
#import "HomeViewController.h"


@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)rememberButtonTouched:(id)sender {
    //[self.rememberButton setSelected:!self.rememberButton.isSelected];
}

- (IBAction)loginButtonTouched:(id)sender {
    // Login and move to home page
    HomeViewController *viewController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    
    // Remove from parent window
    UIViewController *parent = [self parentViewController];
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    [parent.navigationController pushViewController:viewController animated:YES];
    
    parent = nil;
    viewController = nil;
    
}

@end
