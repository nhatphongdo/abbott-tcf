//
//  KnowledgeViewController.h
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TutorViewController.h"

@interface KnowledgeViewController : UIViewController{
    int x;
    TutorViewController *tutorView;
}

@property (weak, nonatomic) IBOutlet UIImageView *bgImage;
@property (weak, nonatomic) IBOutlet UIImageView *bgItem;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;

@property (weak, nonatomic) IBOutlet UILabel *content;
- (IBAction)tummyTestButtonTouched:(id)sender;
- (IBAction)settingsButtonTouched:(id)sender;
- (IBAction)userButtonTouched:(id)sender;

@end
