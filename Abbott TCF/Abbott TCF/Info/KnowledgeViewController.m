//
//  KnowledgeViewController.m
//  Abbott TCF
//
//  Created by Do Nhat Phong on 12/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import "KnowledgeViewController.h"
#import "TummyViewController.h"
#import "AccountViewController.h"


@interface KnowledgeViewController ()

@end

@implementation KnowledgeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    tutorView = [[TutorViewController alloc] initWithNibName:@"TutorViewController" bundle:nil];
    
    [self addChildViewController:tutorView];
    [self.view addSubview:tutorView.view];
    [tutorView didMoveToParentViewController:self];
    
    
    
    // Do any additional setup after loading the view from its nib.
    [self.lbTitle setAlpha:0];
    [self.content setAlpha:0];
    x = self.bgItem.frame.origin.x;
    
    [self.bgItem setFrame:CGRectMake(1000, self.bgItem.frame.origin.y,self.bgItem.frame.size.width,self.bgItem.frame.size.height) ];
    
    [self slideShow];
    
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self closeTutor];
}

-(void)showTutor{
    [tutorView.view setHidden:NO];
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationCurveEaseOut
                     animations:^{
     
                         [tutorView.view setAlpha:1];
                     }
                     completion:^(BOOL finished){
                         [tutorView.view setHidden:NO];
     
                     }];
}

-(void)closeTutor{
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationCurveEaseOut
                     animations:^{
                         
                         [tutorView.view setAlpha:0];
                     }
                     completion:^(BOOL finished){
                         [tutorView.view setHidden:YES];
                         
                     }];
}

- (void)slideShow{
    [UIView animateWithDuration:1.0
                          delay:0
                        options:UIViewAnimationCurveEaseOut
                     animations:^{
                         
                         [self.bgItem setFrame:CGRectMake(x, self.bgItem.frame.origin.y,self.bgItem.frame.size.width,self.bgItem.frame.size.height) ];
                         [self.lbTitle setAlpha:1.0];
                         [self.content setAlpha:1.0];
                         
                     }
                     completion:^(BOOL finished){
                         [self.bgItem setHidden:NO];
                         
                     }];
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)tummyTestButtonTouched:(id)sender {
    TummyViewController *viewController = [[TummyViewController alloc] initWithNibName:@"TummyViewController" bundle:nil];
    [self.navigationController pushViewController:viewController animated:YES];
    [viewController showRegistrationScreen];
    viewController = nil;
}

- (IBAction)settingsButtonTouched:(id)sender {
    [self showTutor];
}

- (IBAction)userButtonTouched:(id)sender {
    AccountViewController *viewController = [[AccountViewController alloc] initWithNibName:@"AccountViewController" bundle:nil];
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
